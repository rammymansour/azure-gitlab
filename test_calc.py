from calc import Calc


class TestClac:
    def test_multiplication(self):
        assert 15 == Calc.multiplication(3, 5)
        assert 25 == Calc.multiplication(5, 5)
        assert 30 == Calc.multiplication(5, 6)

    def test_division(self):
        assert 3 == Calc.division(3, 1)
        assert 2 == Calc.division(8, 4)
        assert 5 == Calc.division(25, 5)

    def test_addtion(self):
        assert 3 == Calc.addtion(2, 1)
        assert 2 == Calc.addtion(1, 1)
        assert 5 == Calc.addtion(2, 3)

    def test_subtraktion(self):
        assert 2 == Calc.subtraktion(3, 1)
        assert 4 == Calc.subtraktion(8, 4)
        assert 20 == Calc.subtraktion(25, 5)
